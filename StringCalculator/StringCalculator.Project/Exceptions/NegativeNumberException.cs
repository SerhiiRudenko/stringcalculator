﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StringCalculator.Exceptions
{
    public class NegativeNumberException : Exception
    {
        public NegativeNumberException(IEnumerable<int> negativeNumbers)
            : base("Negative numbers: " + string.Join(",", negativeNumbers))
        {
        }
    }
}
