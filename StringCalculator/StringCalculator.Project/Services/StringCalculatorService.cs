﻿using StringCalculator.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringCalculator.Project.Services
{
    public class StringCalculatorService
    {
        private char[] Separators = new char[] { ',', '\n' };
        private const string DelimeterIndicator = "//";
        private const char DelimeterMultipleBracketFirst = '[';
        private const char DelimeterMultipleBracketLast = ']';
        private const int TopNumbersLimit = 1000;

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers) || string.IsNullOrWhiteSpace(numbers))
            {
                return 0;
            }

            var customSeparatorsList = new List<string>();
            if (numbers.StartsWith(DelimeterIndicator))
            {
                var firstDiginIndex = numbers.IndexOf(numbers.FirstOrDefault(c => char.IsDigit(c)));
                if (numbers.StartsWith(DelimeterIndicator + DelimeterMultipleBracketFirst))
                {
                    customSeparatorsList.AddRange(
                        numbers.Substring(
                            DelimeterIndicator.Length, 
                            firstDiginIndex- DelimeterIndicator.Length)
                        .Split(DelimeterMultipleBracketFirst, DelimeterMultipleBracketLast, StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Substring(0, x.Length - 1))
                    );
                    numbers = numbers.Remove(0, numbers.LastIndexOf(DelimeterMultipleBracketLast) + 1);
                }
                else
                {
                    customSeparatorsList.Add(
                        numbers.Substring(
                            DelimeterIndicator.Length,
                            firstDiginIndex - DelimeterIndicator.Length
                    ));
                    numbers = numbers.Replace(DelimeterIndicator + customSeparatorsList.Single(), string.Empty);
                }
            }

            var numbersList = !customSeparatorsList.Any()
                ? numbers.Split(Separators)
                : numbers.Split(customSeparatorsList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
 
            var numbersParsed = numbersList.Select(x => int.Parse(x));

            if (numbersParsed.Count() == 1)
            {
                return numbersParsed.Single();
            }

            if (numbersParsed.Any(x => x < 0))
            {
                throw new NegativeNumberException(numbersParsed.Where(x => x < 0));
            }

            var result = 0;
            foreach (var number in numbersParsed)
            {
                if (number > TopNumbersLimit)
                {
                    continue;
                }
                result += number; 
            }
            return result;
        }
    }
}
