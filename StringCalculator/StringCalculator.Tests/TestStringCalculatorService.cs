using StringCalculator.Exceptions;
using StringCalculator.Project.Services;
using System;
using Xunit;

namespace StringCalculator.Tests
{
    public class TestStringCalculatorService
    {
        private readonly StringCalculatorService _stringCalculatorService;
        public TestStringCalculatorService()
        {
            _stringCalculatorService = new StringCalculatorService();
        }

        [Fact]
        public void Add_WhenInputStringEmpty_Return0()
        {
            // act
            var result = _stringCalculatorService.Add("");

            // assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void Add_WhenTheNumberIsPassed_ReturnsThisNumber()
        {
            // act
            var result = _stringCalculatorService.Add("1");

            // assert
            Assert.Equal(1, result);
        }

        [Fact]
        public void Add_WhenTwoNumbersPassed_ReturnsSum()
        {
            // act
            var result = _stringCalculatorService.Add("1,2");

            // assert
            Assert.Equal(3, result);
        }

        [Fact]
        public void Add_SupportNewLineSymbalSeparator()
        {
            // act
            var result = _stringCalculatorService.Add("1,2\n3");

            // assert
            Assert.Equal(6, result);
        }

        [Fact]
        public void Add_NegativeNumbersAreNotAllowed()
        {
            // act assert
            Assert.Throws<NegativeNumberException>(() => _stringCalculatorService.Add("1,-2"));
        }

        [Fact]
        public void Add_NumbersBiggerThen1000AreIgnored()
        {
            // act
            var result = _stringCalculatorService.Add("1001,2");

            // assert
            Assert.Equal(2, result);
        }


        [Fact]
        public void Add_SupportTheCustomStringSeparator()
        {
            // act
            var result = _stringCalculatorService.Add("//***1***2");

            // assert
            Assert.Equal(3, result);
        }

        [Fact]
        public void Add_SupportMultipleStringSeparators()
        {
            // act
            var result = _stringCalculatorService.Add("//[*][%]1*2%3");

            // assert
            Assert.Equal(6, result);
        }
    }
}
